#!/bin/bash
#Define parameters
INSTANCE=i-0abe14977dd160258
ASG_NAME="dbnodeautogroup"
OLD_LC="updateddbmongo"
NEW_LC="dbmongo"
IMAGE_AMI_ID=ami-04af19a0116f592fd
SNAP_ID=snap-0195e147b692e8dbc
REGION=us-east-2
SCALING_POLICY=Add_instance
export existingNumDesiredInstances=$(aws autoscaling --region $REGION describe-auto-scaling-groups --auto-scaling-group-name "$ASG_NAME" --query "AutoScalingGroups[0].DesiredCapacity")

getNumInstancesInAutoScalingGroup() {
    num=$(aws autoscaling --region $REGION  describe-auto-scaling-groups --auto-scaling-group-name "$ASG_NAME" --query "length(AutoScalingGroups[0].Instances)")
    __resultvar=$1
    eval $__resultvar=$num
}


echo 'Create AMI'
IMAGE=`aws ec2 --region $REGION create-image --instance-id $INSTANCE --name NEW-IMAGE --output text`

echo 'Create Launch Configuration'
aws autoscaling --region $REGION create-launch-configuration --launch-configuration-name $NEW_LC --image-id $IMAGE --instance-type t2.micro --key-name vpc --security-groups sg-063f49f8dd3544670

echo 'Update Auto Scaling Group to use new Launch Configuration'
aws autoscaling --region $REGION update-auto-scaling-group --auto-scaling-group-name $ASG_NAME --launch-configuration-name $NEW_LC

echo 'Delete old Launch Configuration'
aws autoscaling --region $REGION delete-launch-configuration --launch-configuration-name $OLD_LC

echo 'Deleting Last Image And SnapShot'
aws ec2 --region $REGION deregister-image --image-id $IMAGE_AMI_ID
aws ec2 --region $REGION delete-snapshot --snapshot-id $SNAP_ID

getNumInstancesInAutoScalingGroup numInstances
numInstancesExpected=$(expr $numInstances \* 2)
echo "Expecting to have $numInstancesExpected instance(s) online."

echo "Will launch $numInstances Instance(s)..."
for i in `seq 1 $numInstances`;
do
    echo "Launching instance..."
    aws autoscaling --region $REGION execute-policy --no-honor-cooldown --auto-scaling-group-name "$ASG_NAME" --policy-name Add_instance
    sleep 60s
done

# Wait for the number of instances to increase
getNumInstancesInAutoScalingGroup newNumInstances
until [ $newNumInstances -eq $numInstancesExpected ]; do
    echo "Only $newNumInstances instance(s) online in $ASG_NAME, waiting for $numInstancesExpected..."
    sleep 10s
    getNumInstancesInAutoScalingGroup newNumInstances
done

echo "Resetting Desired Instances to $existingNumDesiredInstances"
aws autoscaling --region $REGION update-auto-scaling-group --auto-scaling-group-name "$ASG_NAME" --desired-capacity $existingNumDesiredInstances

# Success!
echo "Deployment complete!"